package ru.itis.ruzavin.net;


import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
//		try {
//			URL url = new URL("http://jsonplaceholder.typicode.com/posts?userId=1");
//			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//			Map<String, String> params = new HashMap<>();
//			params.put("key","value");
//			connection.setDoOutput(true);
//			try(DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())){
//				outputStream.writeBytes("userId=1");
//			}
//			connection.setRequestProperty("Content-Type","application/json");
//			connection.setRequestMethod("GET");
//			connection.setConnectTimeout(5000);
//			connection.setReadTimeout(5000);
//			try(BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))){
//				StringBuilder content = new StringBuilder();
//				String input;
//				while ((input = reader.readLine()) != null){
//					content.append(input);
//				}
//				System.out.println(content);
//			}
//			connection.disconnect();
//
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//
//		try {
//			URL postUrl = new URL("http://reqres.in/api/users");
//			HttpURLConnection postConnection = (HttpURLConnection) postUrl.openConnection();
//			postConnection.setRequestMethod("POST");
//			postConnection.setRequestProperty("Content-Type","application/json; utf-8");
//			postConnection.setRequestProperty("Accept","application/json");
//			postConnection.setDoOutput(true);
//			String jsonInputString = "{\n" +
//					"    \"name\": \"morpheus\",\n" +
//					"    \"job\": \"leader\"\n" +
//					"}";
//			try(OutputStream outputStream = postConnection.getOutputStream()){
//				byte[] input = jsonInputString.getBytes("utf-8");
//				outputStream.write(input,0, input.length);
//			}
//			try(BufferedReader reader = new BufferedReader(new InputStreamReader(postConnection.getInputStream()))){
//
//			}
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
		Map<String, String> headers = new HashMap<>();
		Map<String, String> params = new HashMap<>();
		String token = "bfd451e51836e0d869fa50eddb0f19c692a093fbe03503794ae02b4fd6d02821";
		headers.put("Content-Type", "application/json");
		headers.put("Accept", "application/json");
		//headers.put("Authorization", "Bearer " + token);
		params.put("foo","bar");
		params.put("foo1","bar1");
		HttpClient httpClientJavaNetImpl = new HttpClientJavaNetImpl();
		System.out.println(httpClientJavaNetImpl.get("https://postman-echo.com/get", headers, params));
		System.out.println(httpClientJavaNetImpl.post("https://postman-echo.com/post", headers, params));
	}
}
