package ru.itis.ruzavin.net;

import lombok.SneakyThrows;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class HttpClientJavaNetImpl implements HttpClient{

	@SneakyThrows
	@Override
	public String get(String url, Map<String, String> headers, Map<String, String> params) {
		URL getUrl = new URL(createUrlForGetRequest(url,params));
		HttpURLConnection connection = (HttpURLConnection) getUrl.openConnection();

		connection.setRequestMethod("GET");

		for (String header: headers.keySet()){
			connection.setRequestProperty(header,headers.get(header));
		}

		StringBuilder content;
		try(BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()))){
			content = new StringBuilder();
			String input;
			while ((input = bufferedReader.readLine()) != null){
				content.append(input);
			}
		}

		connection.setConnectTimeout(5000);
		connection.setReadTimeout(5000);

		connection.disconnect();

		return content.toString();
	}

	@SneakyThrows
	@Override
	public String post(String url, Map<String, String> headers, Map<String, String> params) {
		URL postUrl = new URL(url);
		HttpURLConnection connection = (HttpURLConnection) postUrl.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);

		for (String header: headers.keySet()){
			connection.setRequestProperty(header,headers.get(header));
		}

		String jsonInputString = createJsonForPostRequest(params);

		try(OutputStream outputStream = connection.getOutputStream()){
			byte[] input = jsonInputString.getBytes(StandardCharsets.UTF_8);
			outputStream.write(input,0,input.length);
		}
		StringBuilder content;
		try(BufferedReader bufferedReader = new BufferedReader(
				new InputStreamReader(connection.getInputStream(),StandardCharsets.UTF_8))){
			content = new StringBuilder();
			String input;
			while ((input = bufferedReader.readLine()) != null) {
				content.append(input.trim());
			}
		}

		return content.toString();
	}

	private String createUrlForGetRequest(String url, Map<String,String> params){
		url += "?";
		StringBuilder urlBuilder = new StringBuilder(url);
		for (String param:params.keySet()){
			urlBuilder.append(param).append("=").append(params.get(param)).append("&");
		}
		return urlBuilder.toString();
	}

	private String createJsonForPostRequest(Map<String,String> params){
		String divider = "\\\"";
		StringBuilder resultString = new StringBuilder("{");
		for (String param:params.keySet()){
			resultString.append(divider).append(param).append(divider).append(":").append(divider)
					.append(params.get(param)).append(divider).append(", ");
		}
		int length = resultString.length();
		resultString.delete(length-2,length-1);
		resultString.append("}");
		return resultString.toString();
	}
}
